FROM node:lts-alpine

RUN mkdir /app
WORKDIR /app

COPY package*.json ./

COPY . ./

RUN npm install

EXPOSE 3000

ENV VITE_BASE_URL="https://crm.itsone.uz/api"
ENV VITE_TOKEN_KEY="access_token"

RUN npm run build

RUN npm install -g serve



CMD ["serve", "-s", "dist"]


