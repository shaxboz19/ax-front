import axios from 'axios';

import { useToken } from 'vue3-shaxboz-auth';

const { removeToken } = useToken();
const axiosIns = axios.create({});

axiosIns.interceptors.response.use((response) => {
  if (!response.data.ok && response.data.code === 1007) {
    removeToken();
    return Promise.reject(response);
  }
  return response;
});

export default axiosIns;
