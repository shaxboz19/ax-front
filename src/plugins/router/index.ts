import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { tokenService } from '@/utils';
const routes: Array<RouteRecordRaw> = [
  {
    path: '/auth',
    component: () => import('@/layouts/auth.vue'),
    children: [
      {
        path: '/auth/sign-in',
        component: () => import('@/views/auth/sign-in.vue'),
        meta: {
          auth: false,
        },
      },
      {
        path: '/auth/sign-up',
        component: () => import('@/views/auth/sign-up.vue'),
        meta: {
          auth: false,
        },
      },
    ],
  },
  {
    path: '/',
    component: () => import('@/layouts/default.vue'),
    children: [
      {
        path: '/contacts',
        component: () => import('@/views/Contacts.vue'),
        meta: {
          auth: true,
        },
      },
      {
        path: '/user',
        component: () => import('@/views/User.vue'),
        meta: {
          auth: true,
        },
      },
    ],
    meta: {
      auth: true,
    },
  },
  {
    path: '/error',
    component: () => import('@/views/Error.vue'),
    meta: {
      auth: true,
    },
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

// router.beforeEach((to, _, next) => {
//   const token = tokenService.getToken();
//     const isAuth = token && token.length > 0;
//     if (to.meta.auth && !isAuth) {
//         next("/auth/sign-in");
//     } else {
//         next();
//     }
// });

export default router;
