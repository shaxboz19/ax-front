import { defineStore } from 'pinia';

interface State {
    pagesTitle: String,
    siderCollapsed: Boolean,
}

export const useCommon = defineStore({
    id: 'common',
    state: (): State => ({
        pagesTitle: "",
        siderCollapsed: false
    }),
    actions: {
        setPagesTitle(value: string) {
            this.pagesTitle = value;
        },
        setSiderCollapsed() {
            this.siderCollapsed = !this.siderCollapsed;
        }
    },
});
