export default interface Filter {
    formatDate: (dateString: string | null) => string;
}