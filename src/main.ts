import { createApp } from 'vue';
import { createPinia } from 'pinia';
import Antd from 'ant-design-vue';

import App from './App.vue';
import router from '@/plugins/router/index';
import PerfectScrollbar from 'vue3-perfect-scrollbar';

// #region styles
import 'ant-design-vue/dist/reset.css';
import '@/assets/fonts/gt-eesti/stylesheet.css';
import 'vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css';
import '@/assets/scss/main.scss';
// #endregion styles

//components
import SvgIcon from '@/components/app/SvgIcon.vue';
import axiosIns from '@/plugins/axios';
import api from '@/plugins/api';

import { authPlugin } from 'vue3-shaxboz-auth';

const pinia = createPinia();
const app = createApp(App);
app.use(api, axiosIns);
app.component('SvgIcon', SvgIcon);

app
  .use(router)
  .use(Antd)
  .use(PerfectScrollbar)
  .use(pinia)
  .use(authPlugin, {
    router: router,
    fetch: axiosIns,
    // baseUrl: import.meta.env.VITE_BASE_URL,
    redirect: {
      login: '/auth/sign-in',
      logout: '/auth/sign-in',
      home: '/',
    },
    local: {
      endpoints: {
        login: { url: '/api/login', method: 'post' },
        logout: { url: '/api/logout', method: 'post' },
        // user: { url: '/auth/user', method: 'get' },
      },
      token: {
        property: 'accessToken',
        type: 'Bearer',
        name: 'Authorization',
      },
    },
  });

app.mount('#app');
