import {Axios} from "axios";
import {Contact} from "@/types/Contacts";

export default ($axios : Axios) => ({
    async getAll(params: any) {
        const data = await $axios.get("/contact", {
            params
        });
        return data;
    },
    async getById(id: string) {
        const {data} = await $axios.get(`/contact/${id}`);
        return data;
    },
    async create(form: Contact) {
        const data = await $axios.post("/contact", form);
        return data;
    },
    async update(id: string, form: Contact) {
        const data = await $axios.patch(`/contact/${id}`, form);
        return data;
    },
    async delete(id: string) {
        await $axios.delete(`/contact/${id}`);

    }

});
