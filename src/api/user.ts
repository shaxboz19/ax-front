import { tokenService } from '@/utils';

export default ($axios: Axios) => ({
  async getUserInfo(form) {
    const { data } = await $axios.post('/api/getUserInfo/', form);
    return data;
  },
});
