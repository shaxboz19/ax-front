import { tokenService } from "@/utils";
import {Axios} from "axios";
import {Auth} from "@/types/Auth";

export default ($axios: Axios ) => ({
    async login(form: Auth) {
        const { data } = await $axios.post("/auth/login/", form);
        const { access_token } = data;
        tokenService.saveToken(access_token);
        return data;
    },

    async register(form: Auth ) {
        const { data } = await $axios.post("/auth/register/", form);
        return data;
    },
    async logout() {
        tokenService.removeToken();
        return;
    },
    async check() {
        const { data } = await $axios.get("/auth/check/");
        return data;
    }
});
