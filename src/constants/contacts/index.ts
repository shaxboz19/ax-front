export const tableColumns =[
    {
        title: "Full Name",
        dataIndex: "fullName",
        key: "fullName",
    },
    {
        title: "Email",
        dataIndex: "email",
        key: "email",
    },
    {
        title: "Phone",
        dataIndex: "phone",
        key: "phone",
    },
    {
        title: "Tags",
        key: "tags",
        dataIndex: "tags",
    },
    {
        title: "Action",
        key: "action",
    },
];

export const tags = [
    {
        title: "Family",
        value: "family",
    },
    {
        title: "Friends",
        value: "friends",
    },
    {
        title: "Work",
        value: "work",
    },
    {
        title: "Other",
        value: "other",
    },
];


export const contactCreateMessages = "Contact created successfully";
export const contactUpdateMessages = "Contact updated successfully";
export const contactDeleteMessages = "Contact deleted successfully";
