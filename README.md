# VContackt
# Для запуска проекта необходимо:
# 1. Создать в корне проекта файл .env и прописать в нем следующие переменные:
VITE_BASE_URL = "https://crm.itsone.uz/api"
VITE_TOKEN_KEY = "access_token"

# 2. Установить зависимости
npm install

# 3. Запустить проект
npm run dev



# Запуск через докер
# 1. Создать в корне проекта файл .env и прописать в нем следующие переменные:
VITE_BASE_URL = "https://crm.itsone.uz/api"
VITE_TOKEN_KEY = "access_token"
# 2. Запустить команду
docker-compose up

# 3. Перейти по адресу
http://localhost:3000
```


